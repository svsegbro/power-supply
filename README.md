# Introduction
This project addresses building your own variable power supply. We distinguish different versions, with increasing building complexity:
* Version 1:  
  * Up-cycle the ATX power supply of an old desktop pc.
  * Three different voltages available: 3.3V, 5V and 12V.
* Version 2:
  * Build on version 1.
  * Introduce additional variability in available voltages. 
* Version 3: 
  * Leave out the ATX. Start from 220V AC. 
  * Only because you can. No real added value compared to version 1 or 2. 

# Version 1
* Take an ATX power supply, which has the following connectors:

![](img/atx-connectors.jpg "Connector overview")

* We will be using the following connectors: 
  * 3.3V, 5V, 12V and GND
  * PS_ON : to be connected to ground to turn on the PSU
  * PWR_OK : power is good
  * VSB : standby (5V)

* Let's start with the following connections: 

![](img/v1-circuit.png "Circuit")

* Note that there are different wires for each of the available voltages. As we don't need all the available current on one single 5V line - 30A is really a lot, let's include some additional USB connectors on the 5V wires. 

## Supply list
* Fuse
  * ...
* Fuse holder: 
  * https://www.gotron.be/zekeringhouder-voor-chassis-6-3-x-32mm-fuse.html
* Power resistor (vermogenweerstand)
  * https://www.gotron.be/componenten/passief/weerstanden/25w-50w/50watt-vermogenweerstand-5-6-57-ohm.html
* Power switch (enkelpolig):
  * https://www.gotron.be/componenten/schakelmat/schakelaars/toestelschakelaar-enkelpolig-on-on-16a-250vac-rood.html
* Heat shrink (krimpkous):
  * https://www.gotron.be/installatie/kabels/krimpkous/set-thermische-krimpkous-10cm-39-pcs-zwart-met-lijm.html
* USB plug:
  * https://www.gotron.be/omkeerbare-usb-gender-changer-type-a-en-b-in-nikkel-d-behuizing.html
* Safety binding posts:
  * https://www.gotron.be/componenten/connectors/banana-plug/safety-binding-post/geisoleerde-stekkerbus-rood-4mm-hirschman.html
* LED with holder:
  * https://www.gotron.be/componenten/actief/led/holder-with-led/led-rood-met-houder-nv-8mm-2v.html

## References
* https://dronebotworkshop.com/atx-bench-supply/

# Version 2
Start from version 1, and introduce a voltage regulator to increase the range of available voltages. This results in two additional phases for our power supply:
* Regulation phase
  * Linear voltage regulators
    * Cheap
    * Waste a lot of energy
    * Produce a lot of heat, requiring a heat sink for cooling 
  * Buck convertors (also called step-down convertor)
    * Use coils
    * May therefore cause interference in your circuits
* Filtering phase:
  * Switching regulators may introduce noise, to be filtered out in this phase.
  * Use capacitors, placed in parallel to the output.
## Supply list
* Linear regulators:
  * LM1084
* Step-down regulators:
  * LM2596
    * https://www.gotron.be/dc-dc-instelbare-step-down-spanningsregelaar-module-lm2596s.html 
    * Limited output current (2.5A)
  * XL4015
    * http://www.xlsemi.com/datasheet/XL4015%20datasheet.pdf
    * Higher current rating than the LM2596 (up to 5 amp)
  * LTC3780
    * Step up and down regulator
    * Output current similar to LM2596
    * https://www.analog.com/media/en/technical-documentation/data-sheets/LTC3780.pdf
* LED volt/amp meter: 
  * https://www.gotron.be/meten-solderen/meettoestel/paneelmeters/paneel-voltmeter-3-5v-30vdc.html
* Linear potentiometer:
  * 200k and 500k for the LTC3780 
* PCB:
  * https://www.gotron.be/meten-solderen/solderen/printplaten/testprint-50x100mm-met-doorlopende-banen.html
  * https://www.gotron.be/printplaat-100x160-3-gaten-per-eiland.html
## References
* https://dronebotworkshop.com/simple-supply/ 
* https://www.instructables.com/id/DIY-Variable-Power-Supply-With-Adjustable-Voltage-/

# Version 3
This version starts from version 2, but leaving out the DC power supply. This means we start from 220V AC, which needs to be lowered first and then rectified (i.e., converted to DC). This introduces two additional phases in our power supply: 
* Transformation:
  * Reduction of a high AC input to a lower AC input, which can be taken in by the voltage regulator.
* Rectification:
  * Converts AC to DC
### Diode bridge
* Acts as a rectifier
* Each diode drops 0.7V when operated in forward bias.
* => Bridge rectification drops 1.4V
## Supply list
* Transformer
  * ...
* Diodes 
  * ...
* Enclosure: 
  * https://www.gotron.be/installatie/montage/metaal/ufo-7-18-19-alu-behuizing-186-x-190-x-72mm.html
  * https://www.gotron.be/installatie/montage/metaal/90-x-310x-210mm.html

## References
* http://maxembedded.com/2013/02/how-to-build-your-own-power-supply/

